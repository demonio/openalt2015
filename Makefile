all:
	mkdir -p bin
	g++ -c src/*.cc -std=c++11
	g++ *.o -o bin/game.linux_64 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
clean:
	rm -Rf bin
	rm *.o
