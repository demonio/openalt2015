// Potřebné knihovny
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

// Čísla do stringů
#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

// Definice potřebných barev
sf::Color Red = sf::Color(255, 0, 0, 255);
sf::Color Blue = sf::Color(63, 124, 182, 255);
sf::Color Black = sf::Color(0,0,0,255);
// Proměné využité ve hře
bool start = false;
bool over = false;
bool play_once = true;
int base = 0;

// Vytvoření okna s určitým rozlišením a jménem
sf::RenderWindow window(sf::VideoMode(800, 600), "OpenAlt 2015");

// Objekt Hráče
class player{
public:
  sf::Texture texture;
  sf::Sprite sprite;
  float speed_x;
  float speed_y;
  float points = 0;
  player();
  void move(float, float);
  void update();
  void draw(sf::RenderWindow *);
};

// Objekt segmentu pro pozadí a překážky
class segment{
public:
  sf::Sprite sprite;
  segment();
  void generateColor();
  void set_position(int, int);
};

// Náhodná barva pro segment
void segment::generateColor(){
  sprite.setColor(sf::Color(std::rand() % 255, std::rand() % 255, std::rand() % 255, 255));
}

// Čtvercová sít segmentů na pozadí hry
class matrix{
public:
  std::vector< std::vector < segment > > seg;
  matrix(int, int);
  void draw(sf::RenderWindow *);
};

// Objekt překážek
class prekazka{
public:
  std::vector< segment > seg;
  void draw(sf::RenderWindow *);
}prek;

// Vykreslení překážek
void prekazka::draw(sf::RenderWindow * window){
    for(int i = 0; i < seg.size();i++)
      window->draw(seg[i].sprite);
}

// Textura pro překážky (čím méně operací s přesunem obrázků 
//             z disku do paměti tím lepe)
sf::Texture texture;

// Konstruktor pro segmenty
segment::segment(){
  sprite.setTexture(texture);
}

// Nastavení pozice jednoho segmentu
void segment::set_position(int x, int y){
  sprite.setPosition(x,y);
}

// Konstruktor sítě obrázků na pozadí
matrix::matrix(int a, int b){
  seg.resize(a);
  for(int i = 0; i< seg.size();i++){
    seg[i].resize(b);
  }
  for(int i = 0; i< seg.size();i++){
    for(int k = 0; k< seg[i].size();k++){
      seg[i][k].set_position(i*70, k*70);
    }
  }
}

// Vykreslení segmentů v mapě
void matrix::draw(sf::RenderWindow * window){
  for(int i = 0; i< seg.size();i++){
    for(int k = 0; k< seg[i].size();k++){
      window->draw(seg[i][k].sprite);
    }
  }
}

// Konstruktor pro inicializaci hráče
player::player(){
  texture.loadFromFile("data/player/postava.png");
  sprite.setTexture(texture);
  sprite.setPosition(400,300);
  speed_x = 0;
  speed_y = 0;
}

// Vykreslení hráče
void player::draw(sf::RenderWindow * window){
  window->draw(sprite);
}

// Nastavení rychlosti hráče v rozmezích
void player::move(float x, float y){
  if(speed_x < 10 and speed_x > - 10)
    speed_x += x;
  if(speed_y < 10 and speed_y > - 10)
    speed_y += y;
  start = true;
}

// Aktualizace hráče
void player::update(){
  float x = sprite.getPosition().x;
  float y = sprite.getPosition().y;

  // Pokud jsme moc v levo nebo v pravo konec hry
  if(x < -70 or x > window.getSize().x)
    over = true;

  // Pokud jsme moc nahuře nebo dole konec hry
  if(y < -70 or y > window.getSize().y)
    over = true;
  
  // Dokud není konec měn pozici podle rychlosti
  if(!over)
    sprite.setPosition(x+speed_x,y+speed_y);

  // Kontrola jestli jsme kolidovali s překažkou
  for(int i = 0;i < prek.seg.size();i++){
    if(prek.seg[i].sprite.getPosition().x < x+35 and prek.seg[i].sprite.getPosition().x+70 > x+35 and
       prek.seg[i].sprite.getPosition().y < y+35 and prek.seg[i].sprite.getPosition().y+70 > y+35)
      over = true;
  }
}

int main(){
    // Nastavit pevný frame rate
    window.setFramerateLimit(60);

    // Obrázek do textury pro segment
    texture.loadFromFile("data/game/2.png");
   
    // Hudba na pozadí
    sf::Music music;
    if (!music.openFromFile("data/audio/com.ogg"))
        return EXIT_FAILURE;

    // Nastavit nekonečnou smičku
    music.setLoop(true);

    // Zapnout hudbu
    music.play();
    
    // Vytvoření mapy
    matrix mapa(1400/70, 1200/70);

    // Zvuk pro game over
    sf::Music music_over;
    if (!music_over.openFromFile("data/audio/game_over.ogg"))
        return EXIT_FAILURE;

    // Font do hry ( SFML nema default font ) 
    sf::Font font;
    if (!font.loadFromFile("data/font.ttf"))
        return EXIT_FAILURE;

    // Text na začatku
    sf::Text text("Press anything to start game", font, 30);
    text.setPosition(200,300);

    // Text pro zobrazení bodů
    sf::Text body("Body:0 ", font, 30);
    body.setColor(Blue);
    body.setPosition(25,25);
    
    // GAME OVEr
    sf::Text game_over("GAME OVER", font, 60);
    game_over.setPosition(100, 250);
    game_over.setColor(Black);

    player hrac;

    //Dokud nezavřeme okno platí true
    while (window.isOpen())
    {
      // Pokud hra začla
      if(start){
	  // Pokud není konec zvedej počer bodů 
	  if(!over)
            hrac.points += 0.01;
	  // Náhodný pohyb aby se postava nezastavila osa x
	  if(hrac.speed_x == 0)
	    // min + (rand() + (int)(max - (min) + 1)
	    hrac.speed_x = -2 + (rand() % (int)(5));
	  // Náhodný pohyb aby se postava nezastavila osa y
	  if(hrac.speed_y == 0)
	    hrac.speed_y = -2 + (rand() % (int)(5));
      }

      // Generování překážek podle bodů
      if((hrac.points - base) > 5){
	base += 5;
	prek.seg.resize(prek.seg.size()+1);
	prek.seg[prek.seg.size()-1].set_position(std::rand() % 800, std::rand() % 600);
	prek.seg[prek.seg.size()-1].generateColor();
      }
      
        // Aktualizace dat hráče 
        hrac.update();
	// Aktualizovat string bodů podle bodů
	body.setString("Body:" + SSTR(int (hrac.points)));

        // Práce s eventy
        sf::Event event;
	// Pokud nějaký event je volán
        while (window.pollEvent(event))
        {
	  // volba mezi eventy
	  switch (event.type){
	    // Zavření okna
	    case sf::Event::Closed:
	     window.close();
	     break;
	    // Pokud je zmáčknuta klávesa
	    case sf::Event::KeyPressed:
	      // Výběr tlačítka
	      switch (event.key.code){
	        case sf::Keyboard::Left:
		  hrac.move(-1, 0);
		  break;
	        case sf::Keyboard::Right:
		  hrac.move(1, 0);
		  break;
	        case sf::Keyboard::Up:
		  hrac.move(0, -1);
		  break;
	        case sf::Keyboard::Down:
		  hrac.move(0, 1);
		  break;
	      }
	      break;
	  }
        }
        // Vyčištění okna
        window.clear(Red);

	// Vykreslení map
	mapa.draw(&window);

	// Vykreslení textů s body
	window.draw(body);
	
	// Vykreslení překážek
	prek.draw(&window);

	// Vykreslení hráče
	hrac.draw(&window);

	// Pokud není začatek napiš uvodní text
	if(!start)
	  window.draw(text);

	// Pokud jsme prohráli
	if(over){
	  // Vykreslení GAME OVER
	  window.draw(game_over);
	  // Přehrát jednou hudbu pro GAME OVEr
	  if(play_once){
	    music.stop();
	    music_over.play();
	    play_once = false;
	  }
	}

	// Zobrazit vše co jsme zatím chtěli
        window.display();
    }

    return EXIT_SUCCESS;
}
